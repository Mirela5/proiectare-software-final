package ro.mirela.feedbackapp;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ro.mirela.feedbackapp.Model.User;
import ro.mirela.feedbackapp.Model.WebServiceMessage;
import ro.mirela.feedbackapp.Repository.Repository;
import ro.mirela.feedbackapp.Services.WebService;

public class CreateAccount extends AppCompatActivity {

    private void showError(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(CreateAccount.this);
                builder.setMessage(R.string.error_sending_data)
                        .setTitle(R.string.quiz_error_title);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }
    public void addUser(String username, String password, int admin) throws IOException, NoSuchAlgorithmException {

       String userJson = new ObjectMapper().writeValueAsString(new User(username,Controller.encodeSHA256(password),admin));
       Button btn = findViewById(R.id.CreateAccount);
        WebService.getInstance().createUser(userJson).enqueue(new Callback<WebServiceMessage>() {
            @Override
            public void onResponse(Call<WebServiceMessage> call, Response<WebServiceMessage> response) {
                WebServiceMessage message = response.body();
                if(message != null && message.response != null && message.response.equals("success") ){
                    startActivity(new Intent(CreateAccount.this, Login.class));
                }
                else if(message != null){
                    btn.setEnabled(true);
                    Toast.makeText(CreateAccount.this,message.error,Toast.LENGTH_LONG).show();

                }
                else{
                    btn.setEnabled(true);
                    Toast.makeText(CreateAccount.this,"Nothing received from server",Toast.LENGTH_LONG).show();

                }
                // todo add error toast
            }

            @Override
            public void onFailure(Call<WebServiceMessage> call, Throwable t) {
                btn.setEnabled(true);
                Toast.makeText(CreateAccount.this,t.getMessage(),Toast.LENGTH_LONG).show();

            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        findViewById(R.id.CreateAccount).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = ((TextView)findViewById(R.id.username)).getText().toString();
                String password = ((TextView)findViewById(R.id.password)).getText().toString();
                int admin = ((CheckBox)findViewById(R.id.admin)).isChecked() ? 1 : 0;
                if(username.trim().isEmpty() ||password.trim().isEmpty()){
                    AlertDialog.Builder builder = new AlertDialog.Builder(CreateAccount.this);
                    builder.setMessage(R.string.login_error_invalid_credentials)
                            .setTitle(R.string.all_fields_required);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                else {

                    try {
                        addUser(username, password,admin);
                    } catch (Exception e) {
                        Toast.makeText(CreateAccount.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }



            }
        });
    }
}
