package ro.mirela.feedbackapp.Services;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WebService {

    private final static String API_URL = "http://192.168.43.184:8080";
            //"http://192.168.43.184:8080";

    private static WebServiceApi webServiceApi;

    public static WebServiceApi getInstance() {
        if (webServiceApi == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(provideOkHttp())
                    .build();

            webServiceApi = retrofit.create(WebServiceApi.class);
        }
        return webServiceApi;
    }

    private static OkHttpClient provideOkHttp() {
        OkHttpClient.Builder httpBuilder = new OkHttpClient.Builder();
        httpBuilder.connectTimeout(30, TimeUnit.SECONDS);

        return httpBuilder.build();
    }

}
