package ro.mirela.feedbackapp.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import ro.mirela.feedbackapp.Model.questions.Question;
import ro.mirela.feedbackapp.R;

public class CreateAnswerVariantRecycleViewAdapter extends RecyclerView.Adapter<CreateAnswerVariantRecycleViewAdapter.ViewHolder> {

   public Question q;
    public List<ViewHolder> views;
    private int pos = 0;
    public CreateAnswerVariantRecycleViewAdapter( Question q){
        this.views = new ArrayList<>();
        this.q = q;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.quizz_create_answer_variant, viewGroup , false);
        ViewHolder h = new ViewHolder(v);
        views.add(h);
        return h;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.variantName.setText(q.answerVariants.get(i));
        viewHolder.variantName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                q.answerVariants.set(i, s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return q.answerVariants.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        EditText variantName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            variantName = itemView.findViewById(R.id.answerVariant);

        }

    }
}
