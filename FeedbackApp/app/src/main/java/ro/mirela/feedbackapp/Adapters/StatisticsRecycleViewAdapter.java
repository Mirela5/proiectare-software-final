package ro.mirela.feedbackapp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ro.mirela.feedbackapp.Model.Statistic;
import ro.mirela.feedbackapp.R;

public class StatisticsRecycleViewAdapter extends RecyclerView.Adapter<StatisticsRecycleViewAdapter.ViewHolder> {

    public List<Statistic> items;
    public Context context;

    public StatisticsRecycleViewAdapter(List<Statistic> items, Context context){
        this.items = items;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.statistic_item, viewGroup , false);

        return new ViewHolder(v,i);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        viewHolder.question.setText(items.get(i).questionText);
        viewHolder.answerVariants.setAdapter(new AnswerVariantRecycleAdapter(items.get(i).answers));
        viewHolder.answerVariants.setLayoutManager(new LinearLayoutManager(context));
        viewHolder.answerVariants.setHasFixedSize(true);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView question;
        public RecyclerView answerVariants;
        public int pos;
        public ViewHolder(@NonNull View itemView, int i) {
            super(itemView);
            question = itemView.findViewById(R.id.questionTitle);
            answerVariants = itemView.findViewById(R.id.answerVariants);
            pos = i;


        }
    }
}
