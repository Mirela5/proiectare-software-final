package ro.mirela.feedbackapp;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ro.mirela.feedbackapp.Model.WebServiceMessage;
import ro.mirela.feedbackapp.Repository.Repository;
import ro.mirela.feedbackapp.Services.WebService;

public class Login extends AppCompatActivity {

    Button loginBtn;


    // daca se apasa pe back afisez mesaj "You need to login first"
    @Override
    public void onBackPressed() {
        Toast.makeText(this, R.string.login_error, Toast.LENGTH_LONG).show();
    }

    View.OnClickListener onLogin = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // preia imputul din cele 2 fields-uri
            String username = ((TextView)findViewById(R.id.username)).getText().toString();
            String password = ((TextView)findViewById(R.id.password)).getText().toString();
            // daca unul din acestea e gol sau contine doar spatii afisez eroare
            if(username.trim().isEmpty() || password.trim().isEmpty()){
                AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
                builder.setMessage(R.string.all_fields_required)
                        .setTitle(R.string.quiz_error_title);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
            // daca nu verific pe server.
            else {
                loginBtn.setEnabled(false);
                try{
                    checkUser(username,password);
                }catch (Exception ex){
                    loginBtn.setEnabled(true);
                }
            }
        }
    };

    private void checkUser(String username, String password) throws NoSuchAlgorithmException {
        WebService.getInstance().login(username,Controller.encodeSHA256(password)).enqueue(new Callback<WebServiceMessage>() {
            @Override
            public void onResponse(Call<WebServiceMessage> call, Response<WebServiceMessage> response) {
                WebServiceMessage message = response.body();
                if(message != null && message.response != null ){
                    Repository.currentToken = message.response;
                    Repository.setUserTypeFromToken();
                    loginBtn.setEnabled(true);
                    startActivity(new Intent(Login.this, ScanQuiz.class));
                }
                else if(message != null){
                    loginBtn.setEnabled(true);
                    Toast.makeText(Login.this,message.error,Toast.LENGTH_LONG).show();

                }
                else{
                    loginBtn.setEnabled(true);
                    Toast.makeText(Login.this,"Nothing received from server",Toast.LENGTH_LONG).show();

                }
                // todo add error toast
            }

            @Override
            public void onFailure(Call<WebServiceMessage> call, Throwable t) {
                loginBtn.setEnabled(true);
                Toast.makeText(Login.this,t.getMessage(),Toast.LENGTH_LONG).show();

            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        loginBtn = findViewById(R.id.loginBTN);
        loginBtn.setOnClickListener(onLogin);

        findViewById(R.id.create_account).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //merge de la activitatea Login la activitatea createAccount;
                startActivity(new Intent(Login.this, CreateAccount.class));
            }
        });
    }
}
