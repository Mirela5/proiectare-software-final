package ro.mirela.feedbackapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.WriterException;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import androidmads.library.qrgenearator.QRGSaver;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ro.mirela.feedbackapp.Adapters.CreateQuestionsRecycleViewAdapter;
import ro.mirela.feedbackapp.Model.Quiz;
import ro.mirela.feedbackapp.Model.WebServiceMessage;
import ro.mirela.feedbackapp.Model.questions.FreeAnswerQuestion;
import ro.mirela.feedbackapp.Model.questions.MultipleAnswerQuestion;
import ro.mirela.feedbackapp.Model.questions.Question;
import ro.mirela.feedbackapp.Repository.Repository;
import ro.mirela.feedbackapp.Services.WebService;

public class CreateQuiz extends AppCompatActivity {

    RecyclerView r;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_create_quiz);
        r = findViewById(R.id.questions);
        Repository.newQuiz = new Quiz();
        r.setAdapter(new CreateQuestionsRecycleViewAdapter(Repository.newQuiz, this));
        r.setLayoutManager(new LinearLayoutManager(CreateQuiz.this));
        r.setHasFixedSize(true);
        findViewById(R.id.addQuestion).setOnClickListener(addQuestion);

    }

    //dialogul de adaugare intrebare
    private View.OnClickListener addQuestion = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            AlertDialog alertDialog = new AlertDialog.Builder(CreateQuiz.this).create();
            alertDialog.setTitle("Info");
            alertDialog.setMessage("What type of question do you want?");
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Single", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Quiz q = ((CreateQuestionsRecycleViewAdapter) Objects.requireNonNull(r.getAdapter())).q;
                    ArrayList<String> answerVariants = new ArrayList<>();
                    answerVariants.add("");
                    q.questions.add(new MultipleAnswerQuestion("", false, answerVariants));
                    r.setAdapter(new CreateQuestionsRecycleViewAdapter(q, CreateQuiz.this));
                    r.setLayoutManager(new LinearLayoutManager(CreateQuiz.this));
                    r.setHasFixedSize(true);
                }
            });

            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Multiple", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Quiz q = ((CreateQuestionsRecycleViewAdapter) Objects.requireNonNull(r.getAdapter())).q;
                    ArrayList<String> answerVariants = new ArrayList<>();
                    answerVariants.add("");
                    q.questions.add(new MultipleAnswerQuestion("", true, answerVariants));
                    r.setAdapter(new CreateQuestionsRecycleViewAdapter(q, CreateQuiz.this));
                    r.setLayoutManager(new LinearLayoutManager(CreateQuiz.this));
                    r.setHasFixedSize(true);
                }
            });


            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Open", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Quiz q = ((CreateQuestionsRecycleViewAdapter) Objects.requireNonNull(r.getAdapter())).q;

                    q.questions.add(new FreeAnswerQuestion(""));
                    r.setAdapter(new CreateQuestionsRecycleViewAdapter(q, CreateQuiz.this));
                    r.setLayoutManager(new LinearLayoutManager(CreateQuiz.this));
                    r.setHasFixedSize(true);
                }
            });
            alertDialog.show();
        }
    };

    @Override
    // cand se creeaza meniul
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create_quiz, menu);
        return true;

    }

    // salveaza local un bitmap si dupa returneaza calea catre el.
    private Uri getBitmapUri(Bitmap bmp) throws IOException {
        File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
        FileOutputStream out = new FileOutputStream(file);

        bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
        out.close();
        // **Warning:** This will fail for API >= 24, use a FileProvider as shown below instead.
        Uri bmpUri = Uri.fromFile(file);
        return bmpUri;
    }

    // validare quiz
    private boolean isValid(Quiz q) {
        int c = 0;
        int qc = 0;
        for (Question qe : q.questions) {
            if (!qe.question.trim().isEmpty())
                qc++;
            c = 0;
            if (qe instanceof MultipleAnswerQuestion)
                for (String a : qe.answerVariants) {
                    if (!a.trim().isEmpty())
                        c++;
                }
            if (!(qe instanceof FreeAnswerQuestion) && c < 2) {
                return false;
            }

        }
        if (qc == 0) {
            return false;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_quiz:
                new AlertDialog.Builder(this)
                        .setTitle("Are you sure you want to save the quiz?")
                        .setMessage("Quizzes can not be edited after they are created.")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                checkQuiz();
                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();
                return true;
            case R.id.cancel_action:
                new AlertDialog.Builder(this)
                        .setTitle("Are you sure you want to cancel the quiz?")
                        .setMessage("This quiz will not be saved.")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                Repository.newQuiz = null;
                                finish();
                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    private void checkQuiz() {
        Quiz q = ((CreateQuestionsRecycleViewAdapter) r.getAdapter()).q;
        // se setaza titlul quizului
        q.name = ((TextView) CreateQuiz.this.findViewById(R.id.quizTitle)).getText().toString();
        // daca e gol afisez eroare
        if (q.name.isEmpty()) {
            showError("Quiz Title is Empty");
        }
        // daca nu e valid afisez eroare
        else if (!isValid(q)) {
            showError("Quiz should contain at least one valid question");
        }
        // daca e valid continui
        else {
            // adaugi in lista quiz-ul
            // il marchezi ca quiz nou
            Repository.newQuiz = q;

            getQuizKey();


        }
    }

    private void getQuizKey() {
        WebService.getInstance().generateQuizKey().enqueue(new Callback<WebServiceMessage>() {
            @Override
            public void onResponse(Call<WebServiceMessage> call, Response<WebServiceMessage> response) {
                WebServiceMessage msg = response.body();
                try {
                    if (msg.error != null)
                        throw new Exception(msg.error);
                    else {
                        Repository.newQuiz.quizKey = msg.response;


                        sendQuiz();
                    }
                } catch (Exception ex) {
                    Toast.makeText(CreateQuiz.this, ex.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<WebServiceMessage> call, Throwable t) {
                Toast.makeText(CreateQuiz.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }

    private void saveQRCode(Bitmap bitmap) throws WriterException, IOException {
        String savePath = getFilesDir().getAbsolutePath();
        QRGSaver.save(savePath, "quiz-" + Repository.newQuiz.quizKey, bitmap, QRGContents.ImageType.IMAGE_JPEG);

        // partajez codul qr
        final Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("image/jpg");
        shareIntent.putExtra(Intent.EXTRA_STREAM, getBitmapUri(bitmap));
        startActivity(Intent.createChooser(shareIntent, "Share image using"));
    }

    private Bitmap getQRCode(int dimensions) throws WriterException {
        QRGEncoder qrgEncoder = new QRGEncoder(Repository.newQuiz.quizKey, null, QRGContents.Type.TEXT, dimensions);
        // deschid dialogul implict android de partajare imagini
        Bitmap bitmap = null;

        // rezolvare problema versiune noua android


        bitmap = qrgEncoder.encodeAsBitmap();
        return bitmap;
    }

    private void applyDeathOnExposureFix() {
        try {
            Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
            m.invoke(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getQRSize() {
        WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int width = point.x;
        int height = point.y;
        int smallerDimension = width < height ? width : height;
        smallerDimension = smallerDimension * 3 / 4;
        return smallerDimension;
    }

    private void sendQuiz() throws IOException {
        Repository.newQuiz.token = Repository.currentToken;
        Repository.newQuiz.timestamp = new Date();
        String quizJson = new ObjectMapper().writeValueAsString(Repository.newQuiz);
        WebService.getInstance().insertQuiz(quizJson).enqueue(new Callback<WebServiceMessage>() {
            @Override
            public void onResponse(Call<WebServiceMessage> call, Response<WebServiceMessage> response) {
                try {
                    int dimensions = getQRSize();
                    applyDeathOnExposureFix();
                    saveQRCode(getQRCode(dimensions));
                    finish();
                } catch (Exception e) {
                    Toast.makeText(CreateQuiz.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<WebServiceMessage> call, Throwable t) {
                Toast.makeText(CreateQuiz.this, t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }

    private void showError(final String error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(CreateQuiz.this);
                builder.setMessage(error)
                        .setTitle(R.string.quiz_error_title);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

    }


}
