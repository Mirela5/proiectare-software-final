package ro.mirela.feedbackapp.Model;

public class User {
    public String username;
    public String password;
    public int type;

    public User(String username, String password, int type) {
        this.username = username;
        this.password = password;
        this.type = type;
    }
}
