package ro.mirela.feedbackapp.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import java.util.ArrayList;
import java.util.List;

import ro.mirela.feedbackapp.R;

public class SingleAnswerRecycleViewAdapter extends RecyclerView.Adapter<SingleAnswerRecycleViewAdapter.ViewHolder> {

    public List<String> items;
    public List<ViewHolder> views;
    private int lastChecked = -1;
    private int pos = 0;
    public SingleAnswerRecycleViewAdapter(List<String> items){
        this.items = items;
        this.views = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.quizz_question_single_item, viewGroup , false);

        ViewHolder h = new ViewHolder(v, pos);

        views.add(h);
        pos++;
        return h;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.answerVariant.setText(items.get(i));
        viewHolder.answerVariant.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked() == true){
                    if(lastChecked != -1)
                        views.get(lastChecked).answerVariant.setChecked(false);

                }
                lastChecked = i;
            }
        });
    }
    public List<String> getAnswer(){
        List<String> l = new ArrayList<>();
        for(ViewHolder v: views){
            if(v.answerVariant.isChecked()){
                l.add(items.get(v.pos));
                return l;

            }
        }
        return null;
    }
    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public RadioButton answerVariant;
        int pos;

        public ViewHolder(@NonNull View itemView, int i) {
            super(itemView);
            answerVariant = itemView.findViewById(R.id.radioButton);
            pos = i;

        }

    }
}
