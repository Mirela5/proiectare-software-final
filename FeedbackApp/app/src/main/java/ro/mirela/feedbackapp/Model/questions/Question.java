package ro.mirela.feedbackapp.Model.questions;



import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonTypeInfo;

import java.util.ArrayList;
import java.util.List;

import static org.codehaus.jackson.annotate.JsonTypeInfo.As.PROPERTY;

@JsonIgnoreProperties({"questionType"})
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,
        include = PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = FreeAnswerQuestion.class),
        @JsonSubTypes.Type(value = MultipleAnswerQuestion.class),
})
public abstract class Question {
    public String question;
    public ArrayList<String> answerVariants;
    boolean multipleAnswer;

    Question() {
        this.answerVariants = new ArrayList<>();
    }

    public abstract String getQuestion();

    public abstract void setQuestion(String question);

    public abstract List<String> getAnswerVariants();

    public abstract void addAnswerVariant(String answerVariant);

    public abstract void removeAnswerVariant(String answerVariant);

    public abstract void updateAnswerVariant(String oldAnswerVariant, String newAnswerVariant);

    public abstract String getQuestionType();

    public abstract boolean getMultipleAnswer();
}
