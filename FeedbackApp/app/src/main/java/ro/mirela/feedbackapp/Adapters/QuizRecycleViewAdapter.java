package ro.mirela.feedbackapp.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ro.mirela.feedbackapp.Model.Quiz;
import ro.mirela.feedbackapp.R;

public class QuizRecycleViewAdapter extends RecyclerView.Adapter<QuizRecycleViewAdapter.ViewHolder> {

    public List<Quiz> items;
    public View.OnClickListener onClickListener;
    public QuizRecycleViewAdapter(List<Quiz> items, View.OnClickListener listener){
        this.items = items;
        this.onClickListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.quiz_recycle_view_item, viewGroup , false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Quiz q = items.get(i);
        viewHolder.quizTitle.setTag(q.quizKey);
        viewHolder.quizTitle.setOnClickListener(onClickListener);
        viewHolder.quizTitle.setText(q.name);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView quizTitle;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            quizTitle = itemView.findViewById(R.id.quiz_title);



        }
    }
}
