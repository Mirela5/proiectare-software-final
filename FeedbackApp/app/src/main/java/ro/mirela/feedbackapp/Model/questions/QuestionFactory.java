package ro.mirela.feedbackapp.Model.questions;

import java.util.ArrayList;

public class QuestionFactory {

    public Question getQuestion(String questionType, String question, boolean multipleAnswer, ArrayList<String> answer_variants) throws InvalidQuestionTypeException {
        if (questionType == null)
            return null;
        else if (questionType.equalsIgnoreCase("free"))
            return new FreeAnswerQuestion(question);
        else if (questionType.equalsIgnoreCase("multiple")) {
            return new MultipleAnswerQuestion(question, multipleAnswer, answer_variants);
        } else {
            throw new InvalidQuestionTypeException("There is no type of exception with name " + questionType);
        }
    }
}
