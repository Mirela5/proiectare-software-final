package ro.mirela.feedbackapp.Model.questions;

public class InvalidQuestionTypeException extends Throwable {
    InvalidQuestionTypeException(String msg) {
        super(msg);
    }
}
