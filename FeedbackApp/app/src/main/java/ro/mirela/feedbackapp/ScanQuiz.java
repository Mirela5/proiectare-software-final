package ro.mirela.feedbackapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ro.mirela.feedbackapp.Model.Quiz;
import ro.mirela.feedbackapp.Model.WebServiceMessage;
import ro.mirela.feedbackapp.Repository.Repository;
import ro.mirela.feedbackapp.Services.WebService;
import ro.mirela.feedbackapp.barcode.BarcodeCaptureActivity;

public class ScanQuiz extends AppCompatActivity {

    public Button startQuizz;

    private static final String LOG_TAG = ScanQuiz.class.getSimpleName();
    private static final int BARCODE_READER_REQUEST_CODE = 1;
    public void getCompletions(){

    }
    public void getDataFromServer(){

    }
    private void showError(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(ScanQuiz.this);
                builder.setMessage(R.string.error_requesting_data)
                        .setTitle(R.string.quiz_error_title);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_quiz);
        // verificam daca utilizatorul e logat. daca nu e afisam logarea
        if(Repository.currentToken.isEmpty()){
            Intent intent = new Intent(this, Login.class);
            startActivity(intent);
            finish();
        }
        // daca e logat aducem datele de pe server
        else{
            getDataFromServer();
        }
        // buton incepere quizz
        startQuizz = findViewById(R.id.takeQuiz);
        startQuizz.setOnClickListener(v -> {
            Intent intent = new Intent(ScanQuiz.this, BarcodeCaptureActivity.class);
            startActivityForResult(intent, BARCODE_READER_REQUEST_CODE);
        });


    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // aici se verifica daca continutul codului QR este valid
        if (requestCode == BARCODE_READER_REQUEST_CODE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {

                if (data != null) {
                    // aduce codul qr din datele extra ale resultatului
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    try {
                        this.getQuizByKey(barcode.displayValue);

                    } catch (Exception ex) {
                        Toast.makeText(this, R.string.invalid_barcode, Toast.LENGTH_LONG).show();
                    }
                }
            } else {
                Log.e(LOG_TAG, String.format(getString(R.string.barcode_error_format),
                        CommonStatusCodes.getStatusCodeString(resultCode)));
                Toast.makeText(this, R.string.no_barcode_captured, Toast.LENGTH_LONG).show();

            }
        }
        else super.onActivityResult(requestCode, resultCode, data);

    }

    private void getQuizByKey(String quizKey) {
        WebService.getInstance().getQuizByKey(quizKey).enqueue(new Callback<WebServiceMessage>() {
            @Override
            public void onResponse(@NonNull Call<WebServiceMessage> call, @NonNull Response<WebServiceMessage> response) {
                WebServiceMessage m = response.body();
                if( m != null && m.response != null) {
                    Quiz q = null;
                    try {
                        q = new ObjectMapper().readValue(m.response, Quiz.class);

                    if (q != null) {
                        // daca e valid mergem la activitatea de completare quiz
                        Intent intent = new Intent(ScanQuiz.this, TakeQuiz.class);
                        Repository.currentQuiz = q;
                        startActivity(intent);
                    } else {
                        Toast.makeText(ScanQuiz.this, R.string.invalid_barcode, Toast.LENGTH_LONG).show();
                    }
                    } catch (IOException e) {
                        Toast.makeText(ScanQuiz.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<WebServiceMessage> call, @NonNull Throwable t) {
                Toast.makeText(ScanQuiz.this, t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
       if(Repository.userType == 1) {
           inflater.inflate(R.menu.menu_scan_quiz, menu);
       }
       else{

           inflater.inflate(R.menu.menu_scan_quiz_normal, menu);

       }

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.view_quizzes:
                Intent intent = new Intent(this,ViewQuizzes.class);
                startActivity(intent);
                return true;
            case R.id.log_out_normal:
            case R.id.log_out:
                Repository.currentToken = "";
                Intent logoutIntent = new Intent(this,Login.class);
                startActivity(logoutIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }
}