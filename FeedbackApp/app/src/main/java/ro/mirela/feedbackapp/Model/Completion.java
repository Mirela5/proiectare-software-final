package ro.mirela.feedbackapp.Model;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Completion {
    public String quizKey;
    public String userToken;
    public Date timestamp;
    public HashMap<String, List<String>> responses;

    public Completion() {
        responses = new HashMap<>();
    }

    public Completion(String currentToken, Quiz quiz, HashMap<String, List<String>> responses) {
        this.quizKey  = quiz.quizKey;
        this.timestamp = new Date();
        this.responses = responses;
        this.userToken = currentToken;
    }
}
