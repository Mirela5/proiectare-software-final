package ro.mirela.feedbackapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ro.mirela.feedbackapp.Adapters.QuizRecycleViewAdapter;
import ro.mirela.feedbackapp.Model.Quiz;
import ro.mirela.feedbackapp.Model.WebServiceMessage;
import ro.mirela.feedbackapp.Repository.Repository;
import ro.mirela.feedbackapp.Services.WebService;

public class ViewQuizzes extends AppCompatActivity {

    private RecyclerView quizList;

    private final View.OnClickListener onClickItem = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(ViewQuizzes.this, ViewStatistics.class);
            Repository.currentQuiz = ((QuizRecycleViewAdapter) Objects.requireNonNull(quizList.getAdapter())).items.stream().filter( i -> i.quizKey == v.getTag()).findFirst().orElse(null);
            startActivity(intent);
        }
    };

    private final View.OnClickListener onClickFloatingButton = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(ViewQuizzes.this, CreateQuiz.class);

            startActivity(intent);
        }
    };

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_quizzes);
        quizList = findViewById(R.id.quizzes);
        findViewById(R.id.addQuiz).setOnClickListener(onClickFloatingButton);
        getQuizzes();
    }

    private void getQuizzes() {
        WebService.getInstance().getQuizzesForToken(Repository.currentToken).enqueue(new Callback<WebServiceMessage>() {
            @Override
            public void onResponse(Call<WebServiceMessage> call, Response<WebServiceMessage> response) {
                WebServiceMessage msg = response.body();
                try{
                    if(msg.error != null)
                        throw new Exception(msg.error);
                    else {
                        List<Quiz> lst = new ObjectMapper().readValue(msg.response,new TypeReference<List<Quiz>>(){});
                        quizList.setAdapter(new QuizRecycleViewAdapter(lst, onClickItem));
                        quizList.setLayoutManager(new LinearLayoutManager(ViewQuizzes.this));
                        quizList.setHasFixedSize(true);

                    }
                }catch (Exception ex){
                    Toast.makeText(ViewQuizzes.this,ex.getMessage(),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<WebServiceMessage> call, Throwable t) {
                Toast.makeText(ViewQuizzes.this,t.getMessage(),Toast.LENGTH_LONG).show();

            }
        });
    }
}
