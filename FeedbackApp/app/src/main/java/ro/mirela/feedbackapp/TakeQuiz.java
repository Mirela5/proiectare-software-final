package ro.mirela.feedbackapp;

import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ro.mirela.feedbackapp.Adapters.QuestionsRecycleViewAdapter;
import ro.mirela.feedbackapp.Model.Completion;
import ro.mirela.feedbackapp.Model.Quiz;
import ro.mirela.feedbackapp.Model.WebServiceMessage;
import ro.mirela.feedbackapp.Repository.Repository;
import ro.mirela.feedbackapp.Services.WebService;

public class TakeQuiz extends AppCompatActivity {

    public Quiz quiz;
    RecyclerView lst;
    @Override
    //creaza layoutul de la quiz
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_quiz);
        lst = findViewById(R.id.questions);
        quiz = Repository.currentQuiz;

        lst.setAdapter(new QuestionsRecycleViewAdapter(quiz.questions,this));
        lst.setLayoutManager(new LinearLayoutManager(this));
        lst.setHasFixedSize(true);

        //click listener pe buton
        findViewById(R.id.finishQuiz).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String,List<String>> responses = ((QuestionsRecycleViewAdapter)lst.getAdapter()).getAllResponses();
                boolean completed = (responses != null);
                if(!completed) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(TakeQuiz.this);
                    builder.setMessage(R.string.quiz_not_all_questions_completed)
                            .setTitle(R.string.quiz_error_title);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                else{
                    Completion c = new Completion(Repository.currentToken, quiz, responses);
                    // send completion to server;
                    try {
                        sendCompletion(c);
                    } catch (IOException e) {

                        Toast.makeText(TakeQuiz.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });



        this.setTitle(quiz.name);
    }

    private void sendCompletion(Completion c) throws IOException {
        String completionJson = new ObjectMapper().writeValueAsString(c);
        WebService.getInstance().insertCompletion(completionJson).enqueue(new Callback<WebServiceMessage>() {
            @Override
            public void onResponse(Call<WebServiceMessage> call, Response<WebServiceMessage> response) {
                    finish();
            }

            @Override
            public void onFailure(@NonNull Call<WebServiceMessage> call, Throwable t) {
                Toast.makeText(TakeQuiz.this, t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

    }

    private void showError(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(TakeQuiz.this);
                builder.setMessage(R.string.error_sending_data)
                        .setTitle(R.string.quiz_error_title);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

}



