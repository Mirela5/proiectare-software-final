package ro.mirela.feedbackapp.Model;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.Date;

import ro.mirela.feedbackapp.Model.questions.Question;

public class Quiz {
    public String quizKey;
    public String name;
    public ArrayList<Question> questions;
    public Date timestamp;
    public int reward;
    public String token;

    public Quiz() {
        this.questions = new ArrayList<>();
    }

    @JsonCreator
    public Quiz(@JsonProperty("quizKey") String quizKey, @JsonProperty("name") String name, @JsonProperty("questions") ArrayList<Question> questions, @JsonProperty("timestamp") Date timestamp, @JsonProperty("reward") int reward,@JsonProperty("token") String token) {
        this.quizKey = quizKey;
        this.name = name;
        this.questions = questions;
        this.timestamp = timestamp;
        this.reward = reward;
        this.token = token;
    }
}
