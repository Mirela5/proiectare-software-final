package ro.mirela.feedbackapp.Services;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import ro.mirela.feedbackapp.Model.User;
import ro.mirela.feedbackapp.Model.WebServiceMessage;

public interface WebServiceApi {
    @POST("validateUser")
    Call<WebServiceMessage> login(@Query("username") String username, @Query("password")  String password);
    @POST("insertUser")
    Call<WebServiceMessage> createUser(@Query("userJson") String userJson);
    @POST("getQuizByKey")
    Call<WebServiceMessage> getQuizByKey(@Query("key") String key);
    @POST("getQuizzesForToken")
    Call<WebServiceMessage> getQuizzesForToken(@Query("token") String token);
    @POST("insertQuiz")
    Call<WebServiceMessage> insertQuiz(@Query("quizJson") String quizJson);
    @POST("generateQuizKey")
    Call<WebServiceMessage> generateQuizKey();
    @POST("getStatisticsForQuiz")
    Call<WebServiceMessage> getStatisticsForQuiz(@Query("key") String key);
    @POST("insertCompletion")
    Call<WebServiceMessage> insertCompletion(@Query("completionJson") String completionJson);
}
