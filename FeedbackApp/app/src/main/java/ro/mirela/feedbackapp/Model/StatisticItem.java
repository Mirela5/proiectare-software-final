package ro.mirela.feedbackapp.Model;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

public class StatisticItem {
    public String answer;
    public long count;
    @JsonCreator
    public StatisticItem(@JsonProperty("answer") String answer, @JsonProperty("count") long count) {
        this.answer = answer;
        this.count = count;
    }
}
