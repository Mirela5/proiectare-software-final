package ro.mirela.feedbackapp.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import java.util.ArrayList;
import java.util.List;

import ro.mirela.feedbackapp.R;

public class MultipleAnswerRecycleViewAdapter extends RecyclerView.Adapter<MultipleAnswerRecycleViewAdapter.ViewHolder> {

    public List<String> items;
    public List<ViewHolder> views;
    private int pos = 0;
    public MultipleAnswerRecycleViewAdapter(List<String> items) {
        this.items = items;
        this.views = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.quizz_question_multiple_item, viewGroup, false);
        ViewHolder h = new ViewHolder(v,pos);
        views.add(h);
        pos++;
        return h;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.answerVariant.setText(items.get(i));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public List<String> getAnswers() {
        List<String> l = new ArrayList<>();
        for(ViewHolder v : views){
            if(v.answerVariant.isChecked()){
                l.add(items.get(v.pos));
            }
        }
        if(l.size() == 0) return null;
        return l;
    }




    public class ViewHolder extends RecyclerView.ViewHolder {
        public CheckBox answerVariant;

        public int pos;
        public ViewHolder(@NonNull View itemView, int i) {
            super(itemView);
            answerVariant = itemView.findViewById(R.id.checkBox);
            pos = i;
        }
    }


}
