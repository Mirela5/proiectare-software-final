package ro.mirela.feedbackapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ro.mirela.feedbackapp.Adapters.QuizRecycleViewAdapter;
import ro.mirela.feedbackapp.Adapters.StatisticsRecycleViewAdapter;
import ro.mirela.feedbackapp.Model.Quiz;
import ro.mirela.feedbackapp.Model.Statistic;
import ro.mirela.feedbackapp.Model.WebServiceMessage;
import ro.mirela.feedbackapp.Repository.Repository;
import ro.mirela.feedbackapp.Services.WebService;

public class ViewStatistics extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_statistics);
        this.setTitle(Repository.currentQuiz.name);

        try {
            getStats();
        } catch (IOException e) {
            Toast.makeText(ViewStatistics.this,e.getMessage(),Toast.LENGTH_LONG).show();
        }


    }

    private void getStats() throws IOException {
        WebService.getInstance().getStatisticsForQuiz(Repository.currentQuiz.quizKey).enqueue(new Callback<WebServiceMessage>() {
            @Override
            public void onResponse(Call<WebServiceMessage> call, Response<WebServiceMessage> response) {
                WebServiceMessage msg = response.body();
                try{
                    if(msg == null)
                        throw new Exception("Invalid Respose From Server");
                    if(msg.error != null)
                        throw new Exception(msg.error);
                    else {

                        List<Statistic> stats = new ObjectMapper().readValue(msg.response,new TypeReference<List<Statistic>>(){});
                        RecyclerView v  = findViewById(R.id.statistics);
                        if(stats.size() > 0) {
                            findViewById(R.id.error).setVisibility(View.GONE);
                            v.setAdapter(new StatisticsRecycleViewAdapter(stats, ViewStatistics.this));
                            v.setLayoutManager(new LinearLayoutManager(ViewStatistics.this));
                            v.setHasFixedSize(true);
                        }
                        else{
                            v.setVisibility(View.GONE);
                        }
                    }
                }catch (Exception ex){
                    Toast.makeText(ViewStatistics.this,ex.getMessage(),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<WebServiceMessage> call, Throwable t) {
                Toast.makeText(ViewStatistics.this,t.getMessage(),Toast.LENGTH_LONG).show();

            }
        });
    }
}
