package ro.mirela.feedbackapp.Model.questions;


import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class MultipleAnswerQuestion extends Question {

    @JsonCreator
    public MultipleAnswerQuestion(@JsonProperty("question") String question, @JsonProperty("multipleAnswer") boolean multipleAnswer, @JsonProperty("answerVariants") ArrayList<String> answerVariants) {
        this.setQuestion(question);

        this.multipleAnswer = multipleAnswer;
        this.answerVariants = answerVariants != null ? answerVariants :  new ArrayList<String>();
    }

    @Override
    public String getQuestion() {
        return question;
    }

    @Override
    public void setQuestion(String question) {
        this.question = question;
    }

    @Override
    public List<String> getAnswerVariants() {
        return answerVariants;
    }

    @Override
    public void addAnswerVariant(String answerVariant) {
        this.answerVariants.add(answerVariant);
    }

    @Override
    public void removeAnswerVariant(String answerVariant) {
        this.answerVariants.remove(answerVariant);
    }

    @Override
    public void updateAnswerVariant(String oldAnswerVariant, String newAnswerVariant) {
        this.answerVariants.add(answerVariants.indexOf(oldAnswerVariant), newAnswerVariant);
        this.answerVariants.remove(oldAnswerVariant);
    }

    @Override
    public String getQuestionType() {
        return "multiple";
    }

    @Override
    public boolean getMultipleAnswer() {
        return multipleAnswer;
    }
}
