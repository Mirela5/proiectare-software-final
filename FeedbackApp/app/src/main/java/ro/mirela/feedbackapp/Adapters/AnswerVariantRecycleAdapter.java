package ro.mirela.feedbackapp.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ro.mirela.feedbackapp.Model.StatisticItem;
import ro.mirela.feedbackapp.R;
// Adapter-ul definste cum se populeaza un View ( EX: RecyclerView)
public class AnswerVariantRecycleAdapter extends RecyclerView.Adapter<AnswerVariantRecycleAdapter.ViewHolder> {

    public List<StatisticItem> items;
    // ViewHolder e o clasa care ajuta la popularea fiecarui item din View
    public List<ViewHolder> views;

    public AnswerVariantRecycleAdapter(List<StatisticItem> items){
        this.items = items;
        this.views = new ArrayList<>();
    }

    @NonNull
    @Override
    // Se apeleaza cand se genereaza View-ul ( adica atunci cand se lanseaza Activty spre executie)
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.statistic_single_item, viewGroup , false);
        ViewHolder h = new ViewHolder(v, i);
        views.add(h);
        return h;
    }

    // se apeleaza dupa ce se generaza View-ul in momentul in care se populeaza datele.
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.answer.setText(items.get(i).answer);
        viewHolder.count.setText(String.format(Locale.ENGLISH,"%d",items.get(i).count));

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        // binding intre xml si java
        public TextView answer;
        public TextView count;
        int pos;

        public ViewHolder(@NonNull View itemView, int i) {
            super(itemView);

            answer = itemView.findViewById(R.id.answer);
            count = itemView.findViewById(R.id.count);
            pos = i;

        }

    }
}
