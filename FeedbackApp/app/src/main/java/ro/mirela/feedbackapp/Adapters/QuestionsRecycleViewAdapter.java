package ro.mirela.feedbackapp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ro.mirela.feedbackapp.Model.Completion;
import ro.mirela.feedbackapp.Model.questions.FreeAnswerQuestion;
import ro.mirela.feedbackapp.Model.questions.MultipleAnswerQuestion;
import ro.mirela.feedbackapp.Model.questions.Question;
import ro.mirela.feedbackapp.R;

public class QuestionsRecycleViewAdapter extends RecyclerView.Adapter<QuestionsRecycleViewAdapter.ViewHolder> {

    public List<Question> items;
    public List<ViewHolder> views;
    Context context;
    int pos = 0;
    public QuestionsRecycleViewAdapter(List<Question> items, Context context){
        this.items = items;
        views = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.question_item, viewGroup , false);
        ViewHolder h = new ViewHolder(v, pos);
        views.add(h);
        pos++;
        return h;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Question q = items.get(i);
        viewHolder.question.setText(q.question);
        if(q instanceof FreeAnswerQuestion){
            viewHolder.answerVariants.setVisibility(View.GONE);
        }
        else if(q instanceof MultipleAnswerQuestion){
            viewHolder.textField.setVisibility(View.GONE);

            viewHolder.answerVariants.setAdapter(new MultipleAnswerRecycleViewAdapter(items.get(i).answerVariants));
            viewHolder.answerVariants.setLayoutManager(new LinearLayoutManager(context));
            viewHolder.answerVariants.setHasFixedSize(true);
        }
        else{
            viewHolder.textField.setVisibility(View.GONE);

            viewHolder.answerVariants.setAdapter(new SingleAnswerRecycleViewAdapter(items.get(i).answerVariants));
            viewHolder.answerVariants.setLayoutManager(new LinearLayoutManager(context));
            viewHolder.answerVariants.setHasFixedSize(true);
        }


    }

    public HashMap<String,List<String>> getAllResponses(){
        HashMap<String,List<String>> c = new  HashMap<>();

        for(ViewHolder v : views){
            List<String> answers = new ArrayList<>();
            RecyclerView.Adapter adapter = v.answerVariants.getAdapter();
            if(adapter == null && !v.textField.getText().toString().trim().isEmpty()){
                    answers.add(v.textField.getText().toString().trim());
            }
            else if(adapter instanceof SingleAnswerRecycleViewAdapter){
                answers = ((SingleAnswerRecycleViewAdapter) adapter).getAnswer();
            }
            else if(adapter instanceof  MultipleAnswerRecycleViewAdapter){
                answers = ((MultipleAnswerRecycleViewAdapter) adapter).getAnswers();
            }
            if(answers == null || answers.isEmpty()) return null;


            c.put(items.get(v.pos).question,answers);
        }

        return c;

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
      public TextView question;
      public EditText textField;
      public RecyclerView answerVariants;
      public int pos;
        public ViewHolder(@NonNull View itemView, int i) {
            super(itemView);
            question = itemView.findViewById(R.id.questionTitle);
            textField = itemView.findViewById(R.id.freeAnswer);
            answerVariants = itemView.findViewById(R.id.answerVariants);
            pos = i;


        }
    }
}
