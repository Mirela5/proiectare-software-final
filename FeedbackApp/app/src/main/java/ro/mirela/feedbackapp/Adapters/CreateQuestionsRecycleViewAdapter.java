package ro.mirela.feedbackapp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ro.mirela.feedbackapp.Model.Quiz;
import ro.mirela.feedbackapp.Model.questions.FreeAnswerQuestion;
import ro.mirela.feedbackapp.Model.questions.Question;
import ro.mirela.feedbackapp.R;

public class CreateQuestionsRecycleViewAdapter extends RecyclerView.Adapter<CreateQuestionsRecycleViewAdapter.ViewHolder> {

    public Quiz q;
    public List<ViewHolder> views;
    Context context;
    int pos = 0;
    public CreateQuestionsRecycleViewAdapter(Quiz items, Context context){
        this.q = items;
        views = new ArrayList<>();
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.quiz_createa_question_item, viewGroup , false);
        ViewHolder h = new ViewHolder(v, pos);
        views.add(h);
        pos++;
        return h;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        final Question q = this.q.questions.get(i);
        viewHolder.title.setText(q.question);
        viewHolder.title.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                q.question = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        if(!(q instanceof FreeAnswerQuestion)){

            viewHolder.answerVariants.setAdapter(new CreateAnswerVariantRecycleViewAdapter(q));
            viewHolder.answerVariants.setLayoutManager(new LinearLayoutManager(context));
            viewHolder.answerVariants.setHasFixedSize(true);
            // listener click pe buton; pentru adaugare varianta raspuns
            viewHolder.btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Question question = ((CreateAnswerVariantRecycleViewAdapter) viewHolder.answerVariants.getAdapter()).q;
                    question.answerVariants.add("");
                    viewHolder.answerVariants.setAdapter(new CreateAnswerVariantRecycleViewAdapter(question));
                }
            });
        }
        else{
            viewHolder.btn.setVisibility(View.GONE);
        }

    }



    @Override
    public int getItemCount() {
        return q.questions.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder{
      public EditText title;
      public RecyclerView answerVariants;
      public Button btn;
      public int pos;
        public ViewHolder(@NonNull View itemView, int i) {
            super(itemView);
            title = itemView.findViewById(R.id.questionTitle);
            answerVariants = itemView.findViewById(R.id.answerVariants);
            btn = itemView.findViewById(R.id.addVariant);
            pos = i;


        }
    }
}
