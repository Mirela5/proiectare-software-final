package ro.mirela.feedbackapp.Repository;


import com.google.android.gms.common.util.Strings;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import ro.mirela.feedbackapp.Model.Completion;
import ro.mirela.feedbackapp.Model.Quiz;
import ro.mirela.feedbackapp.Model.User;

public class Repository {
    public static final String REQUEST_BASE_URL = "http://localhost:8080";
    static public List<Quiz> quizzes = new ArrayList<>();
    static public List<Completion> completions = new ArrayList<>();
    public static String currentToken = "";
    public static Quiz newQuiz;
    public static int userType = 1;
    public static Quiz currentQuiz;


    public static Quiz getQuizByKey(final String quizkey) {
        Optional<Quiz> opt =quizzes.stream().filter(i -> i.quizKey == quizkey).findFirst();
        return opt.orElse(null);
    }

    public static void addCompletion(Completion c) {
        completions.add(c);
    }

    public static void setUserTypeFromToken() {
        String tokenRestore = new String(Base64.getDecoder().decode(Repository.currentToken));
        if(tokenRestore.contains("|")){
           Repository.userType = Integer.parseInt(tokenRestore.split("[|]")[2]);
        }
    }
}
