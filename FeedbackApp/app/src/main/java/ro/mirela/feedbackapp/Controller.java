package ro.mirela.feedbackapp;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public class Controller {
    static String getSHA256String() throws NoSuchAlgorithmException {
        MessageDigest salt;
        salt = MessageDigest.getInstance("SHA-256");
        // geneam un byte array sha-256 (256 biti) pe baza unui guid ranom (unic)
        salt.update(UUID.randomUUID().toString().getBytes(StandardCharsets.UTF_8));
        StringBuilder sb = new StringBuilder();
        // dupa care transformam byte array-ul intr-un string cu format hexa cu 2 cifre
        for (byte b : salt.digest()) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }
    static String encodeSHA256(String string) throws NoSuchAlgorithmException {
        MessageDigest salt;
        salt = MessageDigest.getInstance("SHA-256");
        salt.update(string.getBytes(StandardCharsets.UTF_8));
        StringBuilder sb = new StringBuilder();
        // dupa care transformam byte array-ul intr-un string cu format hexa cu 2 cifre
        for (byte b : salt.digest()) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }
}
