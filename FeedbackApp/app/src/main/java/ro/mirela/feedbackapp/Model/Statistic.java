package ro.mirela.feedbackapp.Model;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class Statistic {
    public String questionText;
    public List<StatisticItem> answers;



    public Statistic(){
        answers = new ArrayList<>();

    }

    @JsonCreator
    public Statistic(@JsonProperty("questionText") String questionText, @JsonProperty("question")List<StatisticItem> answers) {
        this.questionText = questionText;
        this.answers = answers;
    }
}
