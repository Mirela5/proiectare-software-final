package controllers;

import org.apache.tomcat.util.codec.binary.Base64;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Date;
import java.util.UUID;

class MiscController {
    /**
     * Genereaza un string sha256 random
     * @return string-ul sha256
     * @throws NoSuchAlgorithmException
     */
    static String getSHA256String() throws NoSuchAlgorithmException {
        MessageDigest salt;
        salt = MessageDigest.getInstance("SHA-256");
        // geneam un byte array sha-256 (256 biti) pe baza unui guid ranom (unic)
        salt.update(UUID.randomUUID().toString().getBytes(StandardCharsets.UTF_8));
        StringBuilder sb = new StringBuilder();
        // dupa care transformam byte array-ul intr-un string cu format hexa cu 2 cifre
        for (byte b : salt.digest()) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }
    /**
     * Genereaza un token de acces pentru un user
     * @param  uid - id-ul userului
     * @param type - tipul userului
     * @return token-ul de access
     * @throws NoSuchAlgorithmException
     */
    static String generateToken(Long uid, int type) throws SQLException, ClassNotFoundException {

        String token = uid + "|"+ new Date().toString() +"|"+ type;
        return DatabaseController.insertToken(uid,new String(Base64.encodeBase64(token.getBytes())));

    }
    /**
     * Aduce id-ul userului pentur un token;
     * @param token token-ul de acces al user-ului
     * @return id-ul userului
     * @throws NoSuchAlgorithmException
     */
    static Long getUIDFromToken(String token) {

        String tokenRestore = new String(Base64.decodeBase64(token));
        if(tokenRestore.contains("|")){
            return Long.parseLong(tokenRestore.trim().split("[|]")[0]);
        }
        return -1L;



    }
}
