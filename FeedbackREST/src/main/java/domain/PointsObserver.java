package domain;

import java.sql.SQLException;

public interface PointsObserver  {
    void onCompletionInserted(String token, long toAdd) throws SQLException, ClassNotFoundException;
}
