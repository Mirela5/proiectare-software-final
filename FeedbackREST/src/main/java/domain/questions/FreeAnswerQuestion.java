package domain.questions;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class FreeAnswerQuestion extends Question {
    private String question;
    @JsonCreator
    FreeAnswerQuestion(@JsonProperty("question")String question) {
        this.setQuestion(question);
    }

    @Override
    public String getQuestion() {
        return question;
    }

    @Override
    public void setQuestion(String question) {
        this.question = question;
    }

    @Override
    public List<String> getAnswerVariants() {
        return new ArrayList<>();
    }

    /**
     * methods bellow will not be implemented as they are not needed for this.
     *
     * @param answerVariant
     */
    @Override
    public void addAnswerVariant(String answerVariant) {

    }

    @Override
    public void removeAnswerVariant(String answerVariant) {

    }

    @Override
    public void updateAnswerVariant(String oldAnswerVariant, String newAnswerVariant) {

    }

    @Override
    public String getQuestionType() {
        return "free";
    }

    @Override
    public boolean getMultipleAnswer() {
        return false;
    }
}
