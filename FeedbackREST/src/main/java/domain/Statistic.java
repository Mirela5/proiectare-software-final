package domain;

import java.util.ArrayList;
import java.util.List;

public class Statistic {
    public String questionText;
    public List<StatisticItem> answers;

    public Statistic(){
        answers = new ArrayList<>();

    }


    public Statistic(String questionText, List<StatisticItem> answers) {
        this.questionText = questionText;
        this.answers = answers;
    }
}
