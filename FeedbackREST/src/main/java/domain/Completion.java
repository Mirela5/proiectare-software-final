package domain;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Completion {
    public String quizKey;
    public String userToken;
    public Date timestamp;
    public HashMap<String, List<String>> responses;

    public Completion() {

    }
}
