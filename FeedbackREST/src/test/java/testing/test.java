package testing;


import domain.questions.*;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;

public class test {
    @Test
    public void question() {
        QuestionFactory factory = new QuestionFactory();
        ArrayList<String> answers = new ArrayList<>();
        answers.add("1");
        answers.add("2");
        answers.add("3");
        answers.add("4");
        Question q1 = null;
        try {
            q1 = factory.getQuestion("free", "question1", true, answers);

            Assertions.assertTrue(q1 instanceof FreeAnswerQuestion, "This questionText should be FreeAnswer type based on params");
            Assertions.assertFalse(q1.getMultipleAnswer(), "For free answer questions this method should always return false");
            Assertions.assertEquals(0, q1.getAnswerVariants().size(), "There should not be any answer variants for free answer");
            Assertions.assertEquals("question1", q1.getQuestion(), "The questionText must match the parameter");

            q1 = factory.getQuestion("multiple", "question2", true, answers);
            Assertions.assertTrue(q1 instanceof MultipleAnswerQuestion, "This questionText should be Multiple Answer type based on params");
            Assertions.assertTrue(q1.getMultipleAnswer(), "For multiple answer questions this method should always return true");
            Assertions.assertEquals(4, q1.getAnswerVariants().size(), "There should not be any answer variants for free answer");
            Assertions.assertEquals("question2", q1.getQuestion(), "The questionText must match the parameter");

            q1 = factory.getQuestion("multiple", "question3", false, answers);
            Assertions.assertTrue(q1 instanceof MultipleAnswerQuestion, "This questionText should be Single Answer type based on params");
            Assertions.assertFalse(q1.getMultipleAnswer(), "For multiple answer questions this method should always return true");
            Assertions.assertEquals(4, q1.getAnswerVariants().size(), "There should not be any answer variants for free answer");
            Assertions.assertEquals("question3", q1.getQuestion(), "The questionText must match the parameter");

            try {
                q1 = factory.getQuestion("fail", "", false, answers);
                Assertions.fail("This line should not be reached for invalid questionType");
            } catch (InvalidQuestionTypeException ex) {
                Assertions.assertTrue(true);
            }

        } catch (InvalidQuestionTypeException e) {
            e.printStackTrace();
        }
    }


}